FROM golang:1.14

RUN apt-get update

RUN apt-get install -y \
    git make

RUN cd /go/src && \
    git clone https://git.rwth-aachen.de/acs/public/third-party/telegraf-json.git && \
    cd telegraf-json && \
    make go-install

EXPOSE 8125/udp 8092/udp 8094
CMD ["/go/bin/telegraf"]
